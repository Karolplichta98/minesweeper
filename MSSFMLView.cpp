//
// Created by karol on 07.05.2020.
//
#include <SFML/Graphics.hpp>
#include "MSSFMLView.h"
#include "Minesweeper.h"
MSSFMLView::MSSFMLView(MinesweeperBoard &board) :board(board) {
   hidden = sf::RectangleShape(sf::Vector2f(15,15));
    flag = sf::RectangleShape(sf::Vector2f(15,15));
flag.setFillColor(sf::Color(200,0,0));
    mine = sf::RectangleShape(sf::Vector2f(15,15));
    mine.setFillColor(sf::Color(0,0,0));
    hidden.setFillColor(sf::Color(0,200,0));
    hidden.setOutlineThickness(1);
    hidden.setOutlineColor(sf::Color(255, 255, 255));
}
void MSSFMLView::draw(sf::RenderWindow &win) {
    for (int i = 0; i < board.getBoardWidth(); ++i) {
        for (int j = 0; j < board.getBoardHeight(); ++j) {

            int x = 1 + 16 * i;
            int y = 1 + 16 * j;
            if (board.getFieldInfo(i,j) == 'a') {
                hidden.setPosition(x, y);
                win.draw(hidden);
            }

            if (board.getFieldInfo(i,j) == 'F') {
                flag.setPosition(x,y);
                win.draw(flag);
            }
            if (board.getFieldInfo(i,j) == 'X') {
                mine.setPosition(x,y);
                win.draw(mine);
            }

        }
    }
}
