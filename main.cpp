#include <iostream>
#include "Minesweeper.h"
#include "MSBoardTextView.h"
#include "MSTextController.h"
#include <SFML/Graphics.hpp>
#include "MSSFMLView.h"
int main()
{


    MinesweeperBoard board(10, 10, DEBUG);
    board.toggleFlag(2,5);
    board.revealField(3,5);
//board.debug_display();
//    MSBoardTextView view(board);
//    view.display()
    sf::RenderWindow win(sf::VideoMode(800, 600), "My window");
    MSSFMLView view(board);
    while (win.isOpen())
    {

        sf::Event event;
        while (win.pollEvent(event))
        {

            if (event.type == sf::Event::Closed)
                win.close();
        }
view.draw(win);
        win.display();


    }

    return 0;
}