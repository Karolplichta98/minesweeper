//
// Created by karol on 06.05.2020.
//

#ifndef MINESWEPER_MSTEXTCONTROLLER_H
#define MINESWEPER_MSTEXTCONTROLLER_H
#include "Minesweeper.h"
#include "MSBoardTextView.h"
#include <iostream>
class MSTextController {
    MSBoardTextView & view ;
    MinesweeperBoard & board;
public:
    MSTextController(MinesweeperBoard & board ,MSBoardTextView & view);
    void play();

};

#endif //MINESWEPER_MSTEXTCONTROLLER_H