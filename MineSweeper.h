//
// Created by karol on 23.04.20.
//

#ifndef MINESWEEPER_MINESWEEPER_H
#define MINESWEEPER_MINESWEEPER_H
enum GameMode  { DEBUG, EASY, NORMAL, HARD };
enum GameState { RUNNING, FINISHED_WIN, FINISHED_LOSS };
using namespace std;

#include <iostream>
#include <cmath>
#include <ctime>
struct Field
{
    bool hasMine;
    bool hasFlag;
    bool isRevealed;
};

class MinesweeperBoard {
    Field board[100][100];
    int width;
    int height;
    double mine;
    int revealed;

public:

    MinesweeperBoard();
    MinesweeperBoard(int boardwidth,int boardheight,GameMode);
    void debug_display() const;
    void setField(int width,int height,bool hasMine,bool hasFlag,bool isRevealed);
    void setBoard();
    int getBoardWidth() const;
    int getBoardHeight() const;
    int getMineCount() const;
    int countMines(int x, int y) const;
    char getFieldInfo(int x,int y) const;
    bool isRightField(int x,int y) const;
    bool hasFlag(int x,int y);
    void toggleFlag(int x,int y);
    GameState getGameState() const;
    void revealField(int x, int y);
    int getRevealedCount() const;

};


#endif //MINESWEEPER_MINESWEEPER_H