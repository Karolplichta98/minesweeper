//
// Created by karol on 23.04.20.
//

#include "Minesweeper.h"

MinesweeperBoard::MinesweeperBoard() {
    width=20;
    height=20;
    mine=0;
    revealed=0;
};
MinesweeperBoard::MinesweeperBoard(int boardwidth, int boardheight,GameMode mode) {
    height = boardheight;
    width = boardwidth;
    int x;
    int y;
    srand(time(NULL));
    setBoard();
    if (mode == EASY) {
        mine = 0.1 * getBoardWidth() * getBoardHeight();
        for (int i = 0; i < mine ; ++i) {
            x = rand() % getBoardHeight();
            y = rand() % getBoardWidth();
            board[x][y].hasMine=1;
            while (board[height][width].hasMine == true) {
               x = rand() % getBoardWidth();
                y = rand() % getBoardHeight();
            }
        }
    }
    if (mode == NORMAL) {
        mine = 0.2 * getBoardWidth() * getBoardHeight();
        for (int i = 0; i < mine ; ++i) {
            x = rand() % getBoardHeight();
            y = rand() % getBoardWidth();
            board[x][y].hasMine=1;
            while (board[height][width].hasMine == true) {
                x = rand() % getBoardWidth();
                y = rand() % getBoardHeight();
            }
        }
    }
    if (mode == HARD) {
        mine = 0.3 * getBoardWidth() * getBoardHeight();
        for (int i = 0; i < mine ; ++i) {
            x = rand() % getBoardHeight();
            y = rand() % getBoardWidth();
            board[x][y].hasMine=1;
            while (board[height][width].hasMine == true) {
                x = rand() % getBoardWidth();
                y = rand() % getBoardHeight();
            }
        }
    }
    if (mode == DEBUG) {
        for (int i = 0; i <getBoardHeight() ; i=i+2) {
            board[i][0].hasMine=1;
            board[0][i].hasMine=1;
            board[i][i].hasMine=1;
        }
        for (int j = 0; j < getBoardHeight(); ++j) {
            board[j][j].hasMine=1;
        }

        }
    }

void MinesweeperBoard::setBoard() {
    for (int i = 0; i < getBoardWidth(); i++) {
        for (int j = 0; j < getBoardHeight(); j++)
            setField(j,i,0,0,0);
    }
}

;
void MinesweeperBoard::setField(int i, int j, bool hasMine, bool isRevealed, bool hasFlag) {
    board[j][i] = {hasMine, isRevealed, hasFlag};
};

void MinesweeperBoard::debug_display() const {
    for (int i = 0; i <getBoardHeight(); i++) {
        for (int c = 0; c <getBoardWidth(); c++) {
            std::cout << "[";
            if (board[i][c].hasMine) {
                std::cout << "M";
            } else {
                std::cout << ".";
            }
            if (board[i][c].isRevealed) {
                std::cout << "o";
            } else {
                std::cout << ".";
            }
            if (board[i][c].hasFlag) {
                std::cout << "f";
            } else {
                std::cout << ".";
            }
            std::cout << "]";
        }
        std::cout << std::endl;
    }

};
int MinesweeperBoard::getBoardWidth() const {
    return width;
};
int MinesweeperBoard::getBoardHeight() const {
    return height;
};
int MinesweeperBoard::getMineCount() const {
    int Mines=0;
    for (int i = 0; i <getBoardWidth() ; ++i) {
        for (int j = 0; j <getBoardHeight() ; ++j) {
            if (board[j][i].hasMine==1){
                Mines++;
            };
        };
    };
    cout<<endl<<Mines;
    return Mines;
};
int MinesweeperBoard::countMines(int x, int y) const {
    int mines=0;
    if(board[y][x].isRevealed && isRightField(x,y)==1  ) {
        if (board[x - 1][y - 1].hasMine && isRightField(x-1,y-1) ) { mines++; };
        if (board[x][y - 1].hasMine && isRightField(x,y-1)) { mines++; };
        if (board[x][y + 1].hasMine && isRightField(x,y+1)) { mines++; };
        if (board[x - 1][y].hasMine && isRightField(x-1,y)) { mines++; };
        if (board[x - 1][y + 1].hasMine && isRightField(x-1,y+1)) { mines++; };
        if (board[x + 1][y].hasMine && isRightField(x+1,y)) { mines++; };
        if (board[x + 1][y - 1].hasMine && isRightField(x+1,y-1)) { mines++; };
        if (board[x + 1][y + 1].hasMine && isRightField(x+1,y+1)) { mines++; };

        return mines;
    }
    else {
        return 0;
    }

}
char MinesweeperBoard::getFieldInfo(int x, int y) const {
    if (isRightField(x,y)==0 ) {
        return'#';
    }
    if (!board[y][x].isRevealed && board[y][x].hasFlag) {
       return 'F';

    }
    if (!board[y][x].isRevealed && !board[y][x].hasFlag && isRightField(x,y)==1) {
      return 'a';

    }
    if (board[y][x].isRevealed && board[y][x].hasMine) {
       return 'X';
    }
    if (board[y][x].isRevealed && countMines(y,x)==0) {
        std::cout << "0";
    }
    if (board[y][x].isRevealed && countMines(y, x) != 0 && !board[y][x].hasMine) {
        std::cout << countMines(y, x);
    }
}
bool MinesweeperBoard::isRightField(int x,int y) const {
    if (x > width-1 || y > height-1 || x<0 || y<0) {

        return 0;
    } else{

        return 1;
    }
}

bool MinesweeperBoard::hasFlag(int x, int y) {
    if(isRightField(x, y) == 0 || !board[y][x].hasFlag || !board[y][x].isRevealed ) {
//        cout<<"nie";
        return 0;
    }

    else {
//        cout<<"tak";
        return 1;
    }
}
GameState MinesweeperBoard::getGameState() const {
    GameState state=RUNNING;
    for (int x = 0; x < width; ++x) {
        for (int m = 0; m < height; ++m) {
            if (board[m][x].hasMine && board[m][x].isRevealed) {
                state=FINISHED_LOSS;

                return state;
            }
            if (board[m][x].isRevealed == width * height - mine) {
                state=FINISHED_WIN;

                return state;
            }
        }
    }
}
void MinesweeperBoard::toggleFlag(int x, int y) {
    if (getGameState()!=FINISHED_LOSS|| getGameState()!=FINISHED_WIN && !board[y][x].hasFlag && !board[y][x].isRevealed)
        board[y][x].hasFlag=1;
}
void MinesweeperBoard::revealField(int x, int y) {
    srand(time(NULL));
    if (board[y][x].hasMine && getRevealedCount() <= 1 && board[y][x].hasFlag==0) {
        board[y][x].isRevealed = 1;
        board[y][x].hasMine=0;
        while (board[y][x].hasMine == 0) {
            x = rand() % width ;
            y = rand() % height;
            board[y][x].hasMine = 1;
        }
    }
    if (board[y][x].isRevealed==0 && board[y][x].hasMine==0 && board[y][x].hasFlag==0){
        board[y][x].isRevealed=1;
    }
    if (isRightField(x,y)==0){
        return;
    }
    if(getGameState()==FINISHED_WIN||getGameState()==FINISHED_LOSS){
        return;
    }
    if(board[y][x].hasFlag){
        return;
    }
}
int MinesweeperBoard::getRevealedCount() const {
    int Revealed=0;
    for (int i = 0; i < width; ++i) {
        for (int j = 0; j < height; ++j) {
            if (board[j][i].isRevealed) {
                Revealed=Revealed+1;
            }
        }
    }
    return Revealed;
}