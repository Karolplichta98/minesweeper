//
// Created by karol on 07.05.2020.
//

#ifndef MINESWEPER_MSSFMLVIEW_H
#define MINESWEPER_MSSFMLVIEW_H
#include "MSBoardTextView.h"
#include "Minesweeper.h"

class MSSFMLView {

MinesweeperBoard & board;
    sf::RectangleShape hidden;
    sf::RectangleShape flag;
    sf::RectangleShape mine;
public:
    MSSFMLView(MinesweeperBoard & board);
    void draw(sf::RenderWindow &win);
};


#endif //MINESWEPER_MSSFMLVIEW_H