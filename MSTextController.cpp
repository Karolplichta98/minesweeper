//
// Created by karol on 06.05.2020.
//

#include "MSTextController.h"
MSTextController::MSTextController(MinesweeperBoard & board, MSBoardTextView &view) :board(board) , view(view){}
void MSTextController::play() {
    int x;
    int y;
    bool c;
    while (board.getGameState()!=FINISHED_LOSS || board.getGameState()!=FINISHED_WIN){
        view.display();
        std::cout<<"podaj wspolrzedna x"<<endl;
        cin>>x;
        std::cout<<"podaj wspolrzedna y"<<endl;
        cin>>y;
        std::cout<<"podaj znak 0-odkryj,1-flaga ";
        cin>>c;
        if (c==1 ){
            board.toggleFlag(x,y);
        }
        if (c==0) {
            board.revealField(x,y);
        }
    }
}
