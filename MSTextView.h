//
// Created by karol on 01.05.2020.
//

#ifndef MINESWEPER_MSBOARDTEXTVIEW_H
#define MINESWEPER_MSBOARDTEXTVIEW_H
#include "Minesweeper.h"
#include <string>



class MSBoardTextView {
    MinesweeperBoard & board;
public:
    MSBoardTextView(MinesweeperBoard & board);
void display();
};


#endif //MINESWEPER_MSBOARDTEXTVIEW_H