//
// Created by karol on 01.05.2020.
//

#include "MSBoardTextView.h"

void MSBoardTextView::display() {
    for (int i = 0; i < board.getBoardHeight() ; ++i) {
        for (int j = 0; j < board.getBoardWidth(); ++j) {
            std::cout<<"[";
            std::cout<<board.getFieldInfo(j,i);
            std::cout<<"]";
        }
        std::cout<<endl;

    }
    std::cout<<endl;
}
MSBoardTextView::MSBoardTextView(MinesweeperBoard &board) : board(board){}